# SPCOMP

Docker image for compiling sourcepawn scripts

## Usage

Tag image equals sourcemod version
```yaml
image: idk1703/spcomp:1.11

build:
  stage: build
  script:
    - mkdir compiled
    - spcomp example.sp -o=compiled/example.smx
  artifacts:
    name: $CI_PROJECT_NAME-$CI_COMMIT_SHORT_SHA
    paths:
      - compiled/*
```

## Environment Variables

```bash
SM_VERSION=[image tag]
SPCOMP_DIR=[path to spcomp and include dir]
SPCOMP_BIN=[path to spcomp binary]
SPCOMP_INC=[path to include dir]
```
