FROM debian:11-slim AS downloader

RUN apt update && apt install -y --no-install-recommends curl ca-certificates

ARG SM_VERSION='1.11'

WORKDIR /sourcepawn

RUN SM_DROP_URL=https://sm.alliedmods.net/smdrop/$SM_VERSION && \
    SM_LATEST_BUILD=$(curl -s $SM_DROP_URL/sourcemod-latest-linux) && \
    curl -s $SM_DROP_URL/$SM_LATEST_BUILD -o sourcemod.tar.gz

RUN tar xzf sourcemod.tar.gz -C /sourcepawn addons/sourcemod/scripting/include addons/sourcemod/scripting/spcomp64 --strip-components 3 && rm sourcemod.tar.gz

FROM debian:11-slim

LABEL maintainer=idk1703 version="1.1.1.1"

ARG SM_VERSION='1.11'
ENV SM_VERSION=$SM_VERSION
ENV SPCOMP_DIR='/sourcepawn'

COPY --from=downloader /sourcepawn /sourcepawn

ENV SPCOMP_BIN=$SPCOMP_DIR/spcomp64
ENV SPCOMP_INC=$SPCOMP_DIR/include

COPY spcomp.sh $SPCOMP_DIR/spcomp.sh

RUN chmod +x $SPCOMP_DIR/spcomp.sh && ln -s $SPCOMP_DIR/spcomp.sh /bin/spcomp

CMD ["bash"]